<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::namespace('User')->group(function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('post/{post}', 'PostController@post')->name('post');
    Route::get('post/tag/{tag}', 'HomeController@tagPosts')->name('tagPosts');
    Route::get('post/category/{category}', 'HomeController@categoryPosts')->name('categoryPosts');
    Route::post('get-posts', 'PostController@getPosts');
});

Route::prefix('admin')->group(function () {
    Route::namespace('Admin')->group(function () {
        Route::get('home', 'HomeController@index')->name('admin.home');
        Route::resource('users', 'UsersController');
        Route::resource('post', 'PostController');
        Route::resource('tag', 'TagController');
        Route::resource('category', 'CategoryController');
        Route::resource('role', 'RolesController');
        Route::resource('permissions', 'PermissionsController');
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
        Route::post('login', 'Auth\LoginController@login');
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
