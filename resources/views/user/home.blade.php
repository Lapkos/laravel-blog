@extends('user.master')

@section('head')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .fa-thumbs-up:hover {
            color: red;
        }
    </style>
@endsection

@section('bg-img', asset('user/img/home-bg.jpg'))

@section('title', 'Clean Blog')

@section('subtitle', 'A Blog Theme by Start Bootstrap')

@section('main-content')
<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto" id="posts">
            <Post
            v-for="value in blog"
            :title="value.title"
            :subtitle="value.subtitle"
            :created_at="value.created_at"
            :key=value.id
            >
            </Post>
            <!-- Pager -->
            {!! $posts->links() !!}
        </div>
    </div>
</div>

<hr>

@endsection

@section('footer')
    <script src="{{ asset('js/app.js') }}"></script>
@endsection