@extends('user/master')

@section('bg-img', Storage::disk('local')->url($post->image))

@section('title', $post->title)

@section('subtitle', $post->subtitle)

@section('head')
    <link rel="stylesheet" href="{{ asset('user/css/prism.css') }}">
@endsection

@section('main-content')

    <!-- Post Content -->
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <small>Created {{ $post->created_at->diffForHumans() }}</small>
                    @foreach($post->categories as $category)
                        <small class="pull-right" style="margin-left:20px;">
                            <a href="{{ route('categoryPosts', $category->slug) }}">{{ $category->name }}</a>
                        </small>
                    @endforeach
                    {!! htmlspecialchars_decode($post->body) !!}

                    {{--Tags cloud--}}
                    <h3>Tag clouds</h3>
                    @foreach($post->tags as $tag)
                        <small class="pull-left" style="margin-left:10px;border-radius:5px;
                                border:1px solid grey;padding:5px;">
                            <a href="{{ route('tagPosts', $tag->slug) }}">{{ $tag->name }}</a>
                        </small>
                    @endforeach
                </div>
            </div>
        </div>
    </article>

    <hr>

@endsection

@section('footer')
    <script src="{{ asset('user/js/prism.js') }}"></script>
@endsection
