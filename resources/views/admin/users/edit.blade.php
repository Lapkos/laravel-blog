@extends('admin.layouts.master')

@section('headSection')

    <link rel="stylesheet" href="{{ asset('admin/bower_components/select2/dist/css/select2.min.css') }}">

@endsection

@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Text Editors
                <small>Advanced form element</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Editors</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">User</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" action="{{ route('users.update', $user->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}
                            <div class="box-body">
                                <div class="col-lg-6">
                                    @include('includes.errors')
                                    <div class="form-group">
                                        <label for="name">User name</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" placeholder="Enter user name">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">User email</label>
                                        <input type="email" disabled class="form-control" id="email" name="email" value="{{ $user->email }}"placeholder="Enter user email">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone_number">User phone number</label>
                                        <input type="text" class="form-control" id="phone_number" name="phone_number" value="{{ $user->phone_number }}" placeholder="Enter user phone number">
                                    </div>
                                    <div class="form-group">
                                        <label for="roles">User role</label>
                                        <select name="roles[]" id="role" class="form-control select2 select2-hidden-accessible"
                                                multiple="" data-placeholder="Select a state" style="width:100%;" tabindex="-1"
                                                aria-hidden="true">
                                            @foreach ($roles as $role)
                                                <option value="{{ $role->id }}"
                                                        @foreach($user_roles as $user_role)
                                                            @if($user_role->id == $role->id)
                                                                selected
                                                            @endif
                                                        @endforeach
                                                >{{ $role->role }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="status">Status</label>
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="status" value="1"
                                                @if($user->status == '1')
                                                    checked
                                                @endif>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->

                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('footerSection')
    <script src="{{ asset('admin/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endsection