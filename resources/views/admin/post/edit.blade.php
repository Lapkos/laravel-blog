@extends('admin.layouts.master')

@section('headSection')

    <link rel="stylesheet" href="{{ asset('admin/bower_components/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('user/css/prism.css') }}">

@endsection

@section('main-content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Text Editors
                <small>Advanced form element</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Editors</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Title</h3>
                        </div>
                        <!-- /.box-header -->
                        @include('includes.errors')
                        <!-- form start -->
                        <form role="form" action="{{ route('post.update', $post->id) }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="box-body">
                                <div class="col-lg-6">

                                    <div class="form-group">
                                        <label for="title">Post title</label>
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter post title" value="{{ $post->title }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="subtitle">Post Subtitle</label>
                                        <input type="text" class="form-control" id="subtitle" name="subtitle" placeholder="Enter post subtitle" value="{{ $post->subtitle }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="slug">Post Slug</label>
                                        <input type="text" class="form-control" id="slug" name="slug" placeholder="Enter slug" value="{{ $post->slug }}">
                                    </div>

                                </div>

                                <div class="col-lg-6">

                                    <div class="pull-right">
                                        <div class="form-group">
                                            <label for="image">File input</label>
                                            <input type="file" id="image" name="image">
                                        </div>
                                    </div>
                                    <div class="pull-left">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="status" @if($post->status == 1) checked @endif> Publish
                                            </label>
                                        </div>
                                    </div>

                                    <br>
                                    <br>

                                    <div class="form-group" style="margin-top:2%;">
                                        <label for="tags">Tags:</label>
                                        <select name="tags[]" id="tags" class="form-control select2 select2-hidden-accessible"
                                                multiple="" data-placeholder="Select a state" style="width:100%;" tabindex="-1"
                                                aria-hidden="true">
                                            @if(isset($tags) && !empty($tags))
                                                @foreach($tags as $tag)
                                                    <option value="{{ $tag->id }}"
                                                    @foreach($post->tags as $postTag)
                                                        @if($postTag->id == $tag->id)
                                                            selected
                                                        @endif
                                                    @endforeach>{{ $tag->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="categories">Categories:</label>
                                        <select name="categories[]" id="tags" class="form-control select2 select2-hidden-accessible"
                                                multiple="" data-placeholder="Select a state" style="width:100%;" tabindex="-1"
                                                aria-hidden="true">
                                            @if(isset($categories) && !empty($categories))
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}"
                                                        @foreach($post->categories as $postCategory)
                                                            @if($postCategory->id == $category->id)
                                                                selected
                                                            @endif
                                                        @endforeach>{{ $category->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

                                </div>

                                <div class="col-lg-12">

                                    <div class="box">
                                        <div class="box-header">
                                            <h3 class="box-title">Bootstrap WYSIHTML5
                                                <small>Simple and fast</small>
                                            </h3>
                                            <!-- tools box -->
                                            <div class="pull-right box-tools">
                                                <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip"
                                                        title="Collapse">
                                                    <i class="fa fa-minus"></i></button>
                                            </div>
                                            <!-- /. tools -->
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body pad">
                                           <textarea class="textarea" id="editor1" name="body" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $post->body }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a class="btn btn-warning" href="{{ route('post.index') }}">Back</a>
                            </div>
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>

@endsection

@section('footerSection')

    <script src="{{ asset('admin/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('admin/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('user/js/prism.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
    <script>
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
    </script>

@endsection