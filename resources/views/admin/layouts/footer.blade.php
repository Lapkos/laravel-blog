<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
    <script src="{{ asset('admin/bower_components/jquery/dist/jquery.min.js') }}"></script>

    @section('footerSection')
        @show

</footer>
