@extends('user/master')

@section('bg-img', asset('user/img/contact-bg.jpg'))

@section('title', 'Welcome!')

@section('subtitle', '')

@section('main-content')
    <div class="col-lg-12">
        <div class="panel-body col-lg-6 mx-auto">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            You are logged in!
        </div>
    </div>
    <hr>

@endsection
