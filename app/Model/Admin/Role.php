<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    function permissions()
    {
        return $this->belongsToMany('App\Model\Admin\Permissions','permission_role');
    }
}
