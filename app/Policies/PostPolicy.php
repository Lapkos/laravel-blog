<?php

namespace App\Policies;

use App\Model\Admin\Admin;
use App\Model\User\Post;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\Model\Admin\Admin  $admin
     * @return mixed
     */
    public function view(Admin $admin)
    {
        //
    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\Model\Admin\Admin  $admin
     * @return mixed
     */
    public function create(Admin $admin)
    {
        return $this->getAccess($admin, 3);
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\Model\Admin\Admin  $admin
     * @return mixed
     */
    public function update(Admin $admin)
    {
        return $this->getAccess($admin, 4);
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\Model\Admin\Admin  $admin
     * @return mixed
     */
    public function delete(Admin $admin)
    {
        return $this->getAccess($admin, 5);
    }

    protected function getAccess(Admin $admin, $p_id)
    {
        foreach ($admin->roles as $role)
        {
            foreach ($role->permissions as $permission)
            {
                if($permission->id == $p_id)
                {
                    return true;
                }
            }
        }

        return false;
    }
}
