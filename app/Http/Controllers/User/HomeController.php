<?php

namespace App\Http\Controllers\User;

use App\Model\User\Category;
use App\Model\User\Post;
use App\Model\User\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $posts = Post::where('status', 1)->paginate(2);
        return view('user.home', compact('posts'));
    }

    public function tagPosts(Tag $tag)
    {
        $posts = $tag->posts;

        return view('user.home', compact('posts'));
    }

    public function categoryPosts(Category $category)
    {
        $posts = $category->posts;

        return view('user.home', compact('posts'));
    }
}
