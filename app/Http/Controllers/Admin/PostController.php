<?php

namespace App\Http\Controllers\Admin;

use App\Model\User\Category;
use App\Model\User\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User\Post;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();

        return view('admin.post.show', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->can('posts.create'))
        {
            $tags = Tag::all();
            $categories = Category::all();

            return view('admin.post.post', compact('tags', 'categories'));
        }

        return redirect(route('admin.home'))->withErrors('You don\'t have permissions to create post');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'subtitle' => 'required',
            'slug' => 'required',
            'body' => 'required',
        ]);

        $post = new Post;
        if ($request->hasFile('image')) {
            $post->image = $request->image->store('public');
        }
        $post->title = $request->title;
        $post->subtitle = $request->subtitle;
        $post->slug = $request->slug;
        $post->body = $request->body;
        if($request->status == 'on') {
            $post->status = 1;
        }
        else {
            $post->status = 0;
        }
        $post->save();
        $post->tags()->sync($request->tags);
        $post->categories()->sync($request->categories);

        return redirect(route('post.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->can('posts.update'))
        {
            $post = Post::where('id', $id)->first();
            $tags = Tag::all();
            $categories = Category::all();

            return view('admin.post.edit', compact('post', 'categories', 'tags'));
        }

        return redirect(route('admin.home'))->withErrors('You don\'t have permissions to edit post');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'subtitle' => 'required',
            'slug' => 'required',
            'body' => 'required',
        ]);
        $post = Post::find($id);
        if ($request->hasFile('image')) {
            $post->image = $request->image->store('public');
        }
        $post->title = $request->title;
        $post->subtitle = $request->subtitle;
        $post->slug = $request->slug;
        $post->body = $request->body;
        if($request->status == 'on') {
            $post->status = 1;
        }
        else {
            $post->status = 0;
        }
        $post->save();
        $post->tags()->sync($request->tags);
        $post->categories()->sync($request->categories);

        return redirect(route('post.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->can('posts.delete'))
        {
            Post::where('id', $id)->delete();

            return redirect()->back();
        }
        return redirect(route('admin.home'))->withErrors('You don\'t have permissions to delete post');
    }
}
